\documentclass[journal=ancham, manuscript=article, layout=onecolumn]{achemso}
\usepackage{my_style}
\usepackage{polyglossia}
\usepackage{booktabs}

% remove the email address field
\setkeys{acs}{email=false}
% remove star next to the author name
\makeatletter
\def\acs@author@fnsymbol#1{}
\makeatother

\setdefaultlanguage{polish}
% \graphicspath{{../phd/img/}}

\author{dr Mateusz Krzysztof Łącki}
\email{mateusz.lacki@biol.uw.edu.pl}
\affiliation{Wydział Matematyki, Informatyki i Mechaniki Uniwersytetu Warszawskiego}
\title{\Large Metody obliczeniowe i statystyczne analizy danych ze spektrometrów masowych}

\newtheorem{defi}{Definicja}[section]

\begin{document}
\maketitle

Spektrometria mas to dziedzina chemii rozwijająca instrumenty i metody określające molekularną zawartość badanej próbki.
Główne jej narzędzie to spektrometr masowy.
Dostarcza on danych w tak zwanego \textit{widma masowego}, jak na Rysunku~\ref{rys::spektrum substancji P}.
Na osi odciętych odznaczane są stosunki masy do ładunku zaobserwowanych w czasie pomiaru jonów.
Na osi rzędnych pokazane są intensywności jonów -- zwykle proporcjonalne do ich ogólnej liczby.

\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{substancja_P}
  \caption{Widmo masowe Substancji P -- drobnego neuropeptydu.}\label{rys::spektrum substancji P}
\end{figure}

Każdy z atomów danej cząsteczki może wystąpić w kilku wariantach masowych, izotopach, a
ich częstotliwości są dobrze znane\footnote{Bada je Międzynarodowa Unia Chemii Czystej i Stosowanej (IUPAC).}. 
Spektrometr nie rozróżnia pozycji izotopów w ramach czasteczki.
Białko \ce{C_c H_h N_n O_o S_s} generuje wiele sygnałów charakteryzujących się zawartością izotopów,
$\ce{^{12}C_{c_0} ^{13}C_{c_1} ^{1}H_{h_0} ^{2}H_{h_1} ^{14}N_{n_0} ^{15}N_{n_1} ^{16}O_{o_0} ^{17}O_{o_1} ^{18}O_{o_2} ^{32}S_{s_0} ^{33}S_{s_1} ^{34}S_{s_2} ^{36}S_{s_3} }$, gdzie $c = c_0 + c_1$, $h = h_0 + h_1$, i.t.d.
Warianty izotopów różnych atomów zwykle są niezależne od siebie.
Stąd\footnote{Oraz ze znajomości częstotliwości pierwiastków.} wynika, że prawdopodobieństwo wystąpienia molekuły o zadanej zawartości izotopów, tak zwanego \textit{izotopologa}, to produkt rozkładów wielomianowych,
{\footnotesize\begin{equation*}
\binom{c}{c_0, c_1} p_{^{12}\text{C}}^{c_0} p_{^{13}\text{C}}^{c_1}
\binom{h}{h_0, h_1} p_{^{1}\text{H}}^{h_0} p_{\text{D}}^{h_1}
\binom{n}{n_0, n_1} p_{^{14}\text{N}}^{n_0} p_{^{15}\text{N}}^{n_1}
\binom{o}{o_0, o_1, o_2} p_{^{16}\text{O}}^{o_0} p_{^{17}\text{O}}^{o_1} p_{^{18}\text{O}}^{o_2}
\binom{o}{o_0, o_1, o_2} p_{^{32}\text{S}}^{s_0} p_{^{33}\text{S}}^{s_1} p_{^{34}\text{S}}^{s_2} p_{^{36}\text{S}}^{s_3}. 
\end{equation*}}
Masę izotopologa to $m_{^{12}\text{C}} c_0 + m_{^{13}\text{C}} c_1 + \dots + m_{^{36}\text{S}} s_3$, gdzie $m$ to masa danego izotopu.

Wraz z drem M. Starkiem skontruowaliśmy algorytm szybkiego generowania podzbiorów konfiguracji tego rozkładu, \texttt{IsoSpec}.
Przy zadanym łącznym prawdopodobieństwie, generowane podzbiory są najmniejsze z możliwych.
Wygenerowanie wszystkich konfiguracji charakeryzuje złożoność czasowa rzędu $\mathcal{O}( \prod_{e \in \mathcal{E}} n_e^{i_e-1} )$.
Nasz algorytm redukuje złożoność do pierwiastka tej liczby.
Co więcej, wygenerowanie tych elementów jest liniowe względem ich liczby, zatem nasz algorytm jest optymalny.
W praktyce oszczędności są znaczące. 
Dla przykładu, ludzka insulina składa się z ponad $10^{14}$ izotopologów.
Najmniejszy podzbiór o prawdopobieństwie co najmniej $99\%$ ma ich wyłącznie 1716.
To również te konfiguracje mają największą szansę zostać zaobserwowane w obecności szumu.

Powyższe rozważania przeprowadzono dla pojedynczego źródła jonów.
W rzeczywistości, widmo masowe powstaje w wyniku nałożenia sygnałów bardzo wielu cząsteczek, co prowadzi do problemu dekonwolucji (deizotopizacji) sygnałów.
Z problemem tym mierzymy się wykorzystując algorytm \texttt{MassTodon}.
Zakładamy tam znajomość zbioru możliwych do zaobserwowania substancji\footnote{Co odpowiada typowej analizie spektrometrycznej przeprowadzanej przy znajomości badanego gatunku.}.
Działanie \texttt{MassTodon} opiszemy w ramach studium przypadku, którym jest analiza danych pozyskanych metodą \textit{top-down}.
W tej metodzie, substancje zostają pofragmentowane w kontrolowanych warunkach wewnątrz instrumentu\footnote{Tak przeprowadzona fragmentacja pozwala pozyskać informacje na temat przestrzennej struktury białek, jak np. lokalizację post-translacyjnych modyfikacji. Alternatywne metody \textit{bottom-up} polegają na wstępnej lizie białek przed ich analizą: badane fragmenty są lżejsze i trafiają w obszar większej czułości instrumentu.}.
Stowarzyszona grupa belgijsko-niemieckich eksperymentalistów dostarczyła nam danych oraz sugestii dotyczących reakcji, które mogą zachodzić w instrumencie, patrz Tabela~\ref{chemical_reactions}.
W celu uproszczenia analizy, do instrumentu wprowadzono czyste substancje (Subtancja P, Ubikwityna).
Wyfiltrowano następnie prekursory o znanym ładunku i przeprowadzono reakcje fragmentujące w różnych warunkach eksperymentalnych.
Wstępnie przygotowaliśmy listę wszystkich możliwych produktów tych reakcji.
Następnie przeprowadziliśmy analizę składu zawartości widm za pomocą \texttt{MassTodon}'a.
\begin{table*}[t]
\centering
\begin{tabular}{rlcl}
    \textbf{PTR}    &\ce{[M + nH]^{n+} + A^{.-}}    &\ce{->}& \ce{[M + (n-1) H]^{(n-1)+} + AH}  \\
    \textbf{ETnoD}  &\ce{[M + nH]^{n+} + A^{.-}}    &\ce{->}& \ce{[M + nH]^{(n-1)+.} + A}      \\
    \textbf{ETD}    &\ce{[M + nH]^{n+} + A^{.-}}    &\ce{->}& \ce{[c + xH]^{x+} + [z + (n - x)H]^{(n-x-1)+.} + A}\\
    \textbf{HTR}    &\ce{[c + xH]^{x+}}             &\ce{->}& \ce{[c + (x - 1)H]^{x+}}\\
                    &\ce{[z + (n - x)H]^{(n-x-1)+}}     &\ce{->}& \ce{[z + (n - x + 1)H]^{(n-x-1)+}}
\end{tabular}
\caption{Wzory chemiczne rozpatrywanych reakcji. \ce{M} oznacza tu wprowadzoną do instrumentu cząsteczkę.}\label{chemical_reactions}
\end{table*}
\texttt{MassTodon} pobiera listę subtancji chemicznych wraz z ich ładunkami.
Dla każdej substancji z listy generuje 99.9\%-owy zbiór izotopologów.
Izotopologi różnych substancji mogą zajmować bardzo zbliżone fragmenty widma.
W tym przypadku dokonujemy dekonwolucji intensywności sygnałów za pomocą regresji nieujemnej,
lub alternatywnego algorytmu bayesowskiego.
\texttt{MassTodon} raportuje łączne intensywności zaobserwowanych fragmentów.
Tak pozyskane intensywności analizowane są wstępnie za pomocą prostego modelu probabilistycznego, który wylicza prawdopodobieństwa zachodzących reakcji, umożliwiając wstępną szybką ocenę tego, które reakcje dominują.

Alternatywnie, wraz z magistrem M. Ciachem przeprowadziliśmy analizę reportowanych intensywności przez pryzmat teorii reakcji kinetycznych. 
W ramach tego projektu, powstał algorytm \texttt{ETDetective}.
Algorytm ten wykorzystuje teorię różniczkowych równań liniowych w celu przewidzenie stałych reakcji opisujących średnich poziomy obserwowanych intensywności jonów.
Algorytm iteracyjnie dopasowuje się do danych, stopniowo uaktualniając stałe reakcji, zbiegając do stałych minimalizujących błąd.

Wykorzystanie nieujemnej regresji przy dopasowaniu się do danych prowadzi do dobrych wyników, niemniej jest ateoretyczne, a tym samym nieeleganckie.
Od dawna bowiem wykorzystuje się teorię zdarzeń rzadkich do opisu liczby jonów docierających do dektora w spektrometrze.
Teoria ta sugeruje, że liczba jonów powinna być opisana rozkładem poissonowskim.
Udało się nam skutecznie stworzyć prosty algorytm wykorzystujący teorię wnioskowania bayesowskiego do dekonwolucji ilości jonów.
Algorytm ten wykorzystuje prosty próbnik Gibbs'a na sztucznie poszerzonej przestrzeni zdarzeń probabilitycznych.
Ostatecznie, pozwala on określić rozkłady \textit{a posteriori} ilości jonów w próbce.
Rozkłady te mogą służyć dalej analizie danych z widm pod kątem ich zawartości pierwiastków, bając odstępstwa od wartości podanych przez Międzynarodową Unię Chemii Czystej i Stosowanej (IUPAC).

\ornamentheader{publikacje w dziedzienie spektrometrii mas}

\begin{itemize}
  \item Lermyte, F., Łącki, M. K., Valkenborg, D., Gambin, A., \& Sobott, F. (2017). Conformational space and stability of ETD charge reduction products of ubiquitin. Journal of The American Society for Mass Spectrometry, 28(1), 69-76.
  \item  Lermyte, F., Łącki, M. K., Valkenborg, D., Baggerman, G., Gambin, A., \& Sobott, F. (2015). Understanding reaction pathways in top-down ETD by dissecting isotope distributions: A mammoth task. International Journal of Mass Spectrometry, 390, 146-154. 
  \item Ciach, M. A., Łącki, M. K., Miasojedow, B., Lermyte, F., Valkenborg, D., Sobott, F., \& Gambin, A. (2017, May). Estimation of Rates of Reactions Triggered by Electron Transfer in Top-Down Mass Spectrometry. In International Symposium on Bioinformatics Research and Applications (pp. 96-107). Springer, Cham.
  \item Łącki, M. K., Lermyte, F., Miasojedow, B., Startek, M. P., Sobott, F., Valkenborg, D., \& Gambin, A. (2019). \texttt{masstodon}: a tool for assigning peaks and modeling electron transfer reactions in top-down mass spectrometry. Analytical chemistry, 91(3), 1801-1807.
  \item Łącki, M. K., Startek, M., Valkenborg, D., \& Gambin, A. (2017). IsoSpec: Hyperfast Fine Structure Calculator. Analytical Chemistry, 89(6), 3272-3277.
\end{itemize}

\end{document}
